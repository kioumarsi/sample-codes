<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Index;
/**
 * @ORM\Entity
 * @ORM\Table(name="news",indexes={@Index(columns={"title","text","meta_keywords"}, flags={"fulltext"}),@Index(columns={"title","meta_keywords"}, flags={"fulltext"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\NewsRepository")
 */
class News {
    /*
     * .
     * .
     * .
     * more fields
     * .
     * .
     * .
     * .
     * /


      * @ORM\ManyToOne(targetEntity="Source", inversedBy="news")
      * @ORM\JoinColumn(name="source_id", referencedColumnName="id")
      */
    protected $source;

    /*
      * .
      * .
      * .
      * more fields
      * .
      * .
      * .
      * .
      * /
      /**


       * @ORM\Column(type="text", nullable=false)
       */
    protected $metaDescription;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    protected $metaKeywords;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="translations")
     * @ORM\JoinColumn(name="create_user_id", referencedColumnName="id")
     */
    protected $createUser;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="translations")
     * @ORM\JoinColumn(name="translator_id", referencedColumnName="id")
     */
    protected $publishedUser;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="translations")
     * @ORM\JoinColumn(name="editor_id", referencedColumnName="id")
     */
    protected $editor;

    /*
    * .
    * .
    * .
    * more fields
    * .
    * .
    * .
    * .
    * /

    /**
     * @ORM\ManyToMany(targetEntity="Subscription")
     * @ORM\JoinTable(name="news_subscriptions",
     *      joinColumns={@ORM\JoinColumn(name="subscription_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id",onDelete="CASCADE")}
     *      )
     */
    protected $subscriptions;

}