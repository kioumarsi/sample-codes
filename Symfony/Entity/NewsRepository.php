<?php
/**
 * Created by PhpStorm.
 * User: sajad
 * Date: 07.07.16
 * Time: 00:54
 */

namespace AppBundle\Entity;


use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository{

    /**
     * @param $date
     * @param $customUrl
     * @param $locale
     * @return News
     * @author Sajad Kiomarsi (sajad@leslettrespersanes.fr)
     */
    public function getPublishedNews($date,$customUrl,$locale){
        $qb = $this->_em->createQueryBuilder();
        $news = $qb->select('n')
            ->from($this->_entityName, 'n')
            ->where($qb->expr()->like('n.createDate', ':date'))
            ->andWhere($qb->expr()->eq('n.customUrl', ':customUrl'))
            ->andWhere($qb->expr()->eq('n.language', ':language'))
            ->andWhere($qb->expr()->eq('n.status', ':status'))
            ->setParameter('date', $date.'%')
            ->setParameter('customUrl', $customUrl)
            ->setParameter('language', $locale)
            ->setParameter('status','published')
            ->getQuery()->getResult();
        if($news)
            return $news[0];
    }

    /**
     * @param News $news
     * @return mixed
     * @author Sajad Kiomarsi (sajad@leslettrespersanes.fr)
     */
    public function getRelatedNews(News $news){
        $qb = $this->_em->createQueryBuilder();
        $keywords = str_replace(',',' ',$news->getMetaKeywords());
        $query =
            $qb->select('n')
                ->from('AppBundle:News', 'n')
                ->where('n.category=:category')
                ->andWhere('n.status=:status')
                ->setParameter('category',$news->getCategory() )
                ->setParameter('status','published' )
                ->orderBy("MATCH (n.title,n.metaKeywords) AGAINST ('$keywords')")
                ->setMaxResults(5)
                ->getQuery();
        ;
        return $query->getResult();
    }
}