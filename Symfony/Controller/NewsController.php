<?php

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsController extends Controller
{
    /**
     * @param $source
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sajad Kiomarsi (sajad@leslettrespersanes.fr)
     */
    public function sourceAction($source){
        $locale = $this->get('request')->getLocale();
        $em = $this->getDoctrine()->getManager();
        //enable language filter for all queries which come after
        $filter=$em->getFilters()->enable("language");
        $filter->setParameter('locale', $locale);
        $categoryNews = $em->getRepository('AppBundle:News')->findBy(
            array("source"=>$source),
            array("createDate"=>"DESC")
        );
        //end of language filter
        $em->getFilters()->disable("language");
        $source=$em->getRepository('AppBundle:Source')->find($source)->getName();

        return $this->render('AppBundle:Frontend:News/index.html.twig',
            array(
                "category"=>$source,
                "categoryNews"=>$categoryNews
            ));
    }

    /**
     * @param $year
     * @param $month
     * @param $day
     * @param $customUrl
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sajad Kiomarsi (sajad@leslettrespersanes.fr)
     */
    public function detailAction($year, $month, $day, $customUrl)
    {
        $locale = $this->get('request')->getLocale();
        $needSubscription = false;
        $firstParagraph = null;
        $user = $this->getUser();
        $news = $this->getDoctrine()->getRepository('AppBundle:News')->getPublishedNews($year.'-'.$month.'-'. $day,$customUrl,$locale);
        if($news === null) {
            return $this->redirectToRoute('frontend_home');
        }
        $relatedNews = $this->getDoctrine()->getRepository('AppBundle:News')->getRelatedNews($news);
        
        //to be have a better adjustment in the view which has different layout for each related news
        $relatedNews = $this->get('app.news.summary_helper')->sortByLength($relatedNews);
        if ($news && count($news->getSubscriptions()) > 0 &&
            (!$user || (!$this->isGranted('ROLE_TRANSLATOR') && 
            (!$user->hasValidSubscription() || (!$news->getSubscriptions()->contains($user->getSubscription())))))
        ) {
            $needSubscription = true;
            $firstParagraph = substr($news->getText(), 0, strpos($news->getText(), "</p>") + 4);
        }
        $image='/media/'.$year."/".$month."/".$day."/thumb-xLarge_".$news->getThumbnail();
        
        return $this->render('AppBundle:Frontend:News/detail.html.twig',
            array(
                'news' => $news,
                'needSubscription' => $needSubscription,
                'firstParagraph' => $firstParagraph,
                'image'=>$image,
                'relatedNews'=>$relatedNews,
            )
        );
    }
}