<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/*
 * To handle the requests base on domain name and set the correct locale to the website
 */
class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;
    private $locale;
    public function __construct($defaultLocale = 'en')
    {
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @param GetResponseEvent $event
     * @author Sajad Kiomarsi (sajad@leslettrespersanes.fr)
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $host = $request->getHttpHost();
        $TLD = substr($host, strrpos($host, '.'));


        switch ($host) {
            case 'www.leslettrespersanes.fr':
            case 'leslettrespersanes.fr':
            case 'lettrespersanes.com':
            case 'lettrespersanes.dev':
            case 'v2.lettrespersanes.com':
            case 'dev.fr.leslettrespersanes.fr':
                $locale = 'fr';
                break;
            case 'persischeberife.de':
            case 'persischebriefe.dev':
                $locale = 'de';
                break;
            default:
                $locale = $this->defaultLocale;
        }

        $request->setLocale($locale);
        $this->locale=$locale;
    }

    /**
     * @return array
     * @author Sajad Kiomarsi (sajad@leslettrespersanes.fr)
     */
    public static function getSubscribedEvents()
    {
        return array(
            // must be registered after the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 15)),
        );
    }
    
    public function getLocale(){
        return $this->locale;
    }
}
