<?php
namespace AppBundle\Filter;
use Doctrine\ORM\Mapping\ClassMetaData,
    Doctrine\ORM\Query\Filter\SQLFilter;

class LanguageFilter extends SQLFilter
{

    /**
     * @param ClassMetaData $targetEntity
     * @param $targetTableAlias
     * @return string
     * @author Sajad Kiomarsi (sajad@leslettrespersanes.fr)
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {

        return $targetTableAlias.'.language = ' . $this->getParameter('locale'); // getParameter applies quoting automatically
    }
}